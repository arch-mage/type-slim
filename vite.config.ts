/// <reference types="vitest" />

import { copyFileSync } from 'node:fs'
import { defineConfig } from 'vite'
import dts from 'vite-plugin-dts'
import { globbySync } from 'globby'

const viteConfig = defineConfig({
  plugins: [
    dts({
      entryRoot: 'src',
      staticImport: true,
      exclude: ['**/*.test.ts', '**/*.test-d.ts', '*.config.ts'],
      afterBuild: () => {
        globbySync(['dist/**/*.d.ts', 'dist/**.d.ts']).map((file) => {
          copyFileSync(file, file.replace(/\.d\.ts$/, '.d.cts'))
        })
      },
    }),
  ],
  build: {
    target: 'esnext',
    minify: false,
    sourcemap: true,
    emptyOutDir: true,
    lib: {
      entry: globbySync(['src/**/*.ts', '!**/*.test.ts', '!**/*.test-d.ts']),
    },
    rollupOptions: {
      external: [/^(?:@[a-z0-9-*~][a-z0-9-*._~]*\/)?[a-z0-9-~]/],
      output: [
        {
          format: 'cjs',
          preserveModules: true,
          preserveModulesRoot: 'src',
          exports: 'named',
          entryFileNames: '[name].cjs',
        },
        {
          format: 'es',
          preserveModules: true,
          preserveModulesRoot: 'src',
          exports: 'named',
          entryFileNames: '[name].js',
        },
      ],
    },
  },
  test: {
    watch: false,
    typecheck: {
      enabled: true,
    },
    include: ['src/**/*.test.ts', 'src/**/*.test-d.ts'],
    coverage: {
      enabled: true,
      exclude: [
        'src/**/*.test.ts',
        'src/**/*.test-d.ts',
        'src/**/index.ts',
        '*.ts',
      ],
    },
  },
})

export default viteConfig
