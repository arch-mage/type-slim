import * as fs from 'node:fs'

const files = fs
  .readdirSync('src')
  // .map((file) => path.join('src', file))
  .filter(
    (name) =>
      name.endsWith('.ts') &&
      !name.endsWith('.test.ts') &&
      !name.endsWith('.test-d.ts'),
  )

const exports = {} as Record<
  string,
  {
    types: `${string}.d.ts`
    import: `${string}.js`
    require: `${string}.cjs`
  }
>

for (const file of files) {
  const name = file.slice(0, -3)
  if (name === 'index') {
    exports[`.`] = {
      types: `./dist/index.d.ts`,
      import: `./dist/index.js`,
      require: `./dist/index.cjs`,
    }
  } else {
    exports[`./${name}`] = {
      types: `./dist/${name}.d.ts`,
      import: `./dist/${name}.js`,
      require: `./dist/${name}.cjs`,
    }
  }
}

const pkg = JSON.parse(fs.readFileSync('package.json', 'utf-8'))

pkg.main = './dist/index.cjs'
pkg.module = './dist/index.js'
pkg.types = './dist/index.d.ts'
pkg.exports = exports

fs.writeFileSync('package.json', JSON.stringify(pkg, null, 2))
