import { expectTypeOf, test } from 'vitest'
import assert from './assert'
import isString from './isString'
import type { Assert } from './assert'

test('assert', () => {
  const something: unknown = null
  assert(isString, 'must be a string', something)
  expectTypeOf(something).toEqualTypeOf<string>()

  // Type guard must be explicitly specified for assertion to work
  // https://stackoverflow.com/questions/64297259/how-to-resolve-assertions-require-every-name-in-the-call-target-to-be-declared/72689922#72689922
  // https://github.com/microsoft/TypeScript/issues/34523
  const mustString: Assert<string> = assert(isString, 'must be a string')
  mustString(something)
})
