/**
 * Create function to check whether value is the same as the given value. The
 * function just uses `Object.is` to compare. The purpose of this function is to
 * create a type guard for a specific value. That's why the type of `value`
 * should be a literal string, number or boolean.
 *
 * @param value - the unknown value.
 */
export function isConstOf<T extends string | number | boolean>(
  value: T,
): (value: unknown) => value is T {
  return (val: unknown): val is T => Object.is(val, value)
}
