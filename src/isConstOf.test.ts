import { expect, test } from 'vitest'
import { isConstOf } from './isConstOf'

test('isValueOf', () => {
  const isPositiveZero = isConstOf(+0)
  const isNegativeZero = isConstOf(-0)

  expect(isPositiveZero(+0)).toBeTruthy()
  expect(isNegativeZero(-0)).toBeTruthy()
  expect(isPositiveZero(-0)).toBeFalsy()
  expect(isNegativeZero(+0)).toBeFalsy()
})
