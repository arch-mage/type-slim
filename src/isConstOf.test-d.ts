import { expectTypeOf, test } from 'vitest'
import { isConstOf } from './isConstOf'

test('isValueOf', () => {
  const isJohnDoe = isConstOf('John Doe')
  const isTrue = isConstOf(true)
  const isZero = isConstOf(0)

  const something: unknown = null

  if (isJohnDoe(something)) {
    expectTypeOf(something).toEqualTypeOf<'John Doe'>()
  } else {
    expectTypeOf(something).not.toEqualTypeOf<'John Doe'>()
  }

  if (isTrue(something)) {
    expectTypeOf(something).toEqualTypeOf<true>()
  } else {
    expectTypeOf(something).not.toEqualTypeOf<true>()
  }

  if (isZero(something)) {
    expectTypeOf(something).toEqualTypeOf<0>()
  } else {
    expectTypeOf(something).not.toEqualTypeOf<0>()
  }
})
